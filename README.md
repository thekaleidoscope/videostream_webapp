# VideoStream-WebApp

This is a react-redux hello-world app, it uses components to define and manipulate states.

## What it does?
   The page by default displays some videos with seach term of `Pizza`.
   The user can enter any search input in the search-bar and the result is changes as such.
   
### Tech-Stack

  * React-Redux Boilerplate
  * [create-react-app scripts] (https://github.com/gitname/react-gh-pages)
  * Node-Modules
  * [react-scripts] (https://www.npmjs.com/package/react-scripts)
  * [GitPage] (https://thekaleidoscope.github.io/videostream_webapp/)
  

---
## Instalation

### Getting Started

There are two methods for getting started with this repo.

#### Familiar with Git?
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone https://github.com/thekaleidoscope/videostream_webapp.git
> cd videostream_webapp
> npm install
> npm start
```

#### Not Familiar with Git?
Click [here](https://github.com/thekaleidoscope/videostream_webapp/archive/master.zip) then download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
> npm install
> npm start
```
## Help-Needed
   **If you are familier with deploying a react app on git pages please check out the issue posted.**
