import React from 'react';

const VideoDetails = ({video}) => {
    if(!video){
        return <div>Loading.....</div>
    }
    const VI=video.id.videoId;
    const url=`https:/youtube.com/embed/${VI}`;

    return(
        <div className="video-details col-md-8">
            <div className="embed-responsive embed-responsive-16by9">
                <iframe className="embed-responsive-item" src={url}></iframe>
            </div>

            <div>{video.snippet.title}</div>

            <div>{video.snippet.description}</div>
        </div>

    );
};

export default VideoDetails;
