import _ from 'lodash';
import React , {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideosList from './components/video_list';
import VideoDetails from './components/video_details';
const Youtube_API= 'AIzaSyBKyLFg4IN28nJ-6FpGwMyQoAyjAf4zuVg';


class App extends Component
{
    constructor(props)
    {
        super(props);

        this.state={ videos:[] , selectedVideo:null };

        this.videoSearch('Pizza');
    }
    videoSearch(term){
        YTSearch({key:Youtube_API, term: term},(Videos)=>{
            this.setState({
                videos:Videos,
                selectedVideo:Videos[0]
            });
        });
    }
    render()
    {   const videosearch= _.debounce( (nterm) => { this.videoSearch(nterm)} , 300);
        return (
            <div>
            <SearchBar onVideoSearch={videosearch}/>
            <VideoDetails video={this.state.selectedVideo} />
            <VideosList
            onVideoSelected = {(newVideo) => this.setState({selectedVideo:newVideo}) }
             videos={this.state.videos} />
            </div>
                );
    }

}


ReactDOM.render(<App />,document.querySelector('.container'));
